https://gitlab.com/ahmetahunov/jse-10
# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven 4.0

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/jse-10.git
cd jse-10
mvn clean install
```

## run app
```bash
java -jar target/release/bin/taskmanager.jar
```
