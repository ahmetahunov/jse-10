package ru.ahmetahunov.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.enumerated.Status;
import java.util.Date;

public interface IItem {

    @NotNull
    public String getId();

    @NotNull
    public String getName();

    @NotNull
    public String getDescription();

    @NotNull
    public Date getStartDate();

    @NotNull
    public Date getFinishDate();

    @NotNull
    public Date getCreationDate();

    @NotNull
    public Status getStatus();

}
