package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Task;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    public void removeAll(String userId);

    @NotNull
    public List<Task> findAll(String userId);

    @NotNull
    public List<Task> findAll(String userId, Comparator<Task> comparator);

    @NotNull
    public List<Task> findAll(String projectId, String userId);

    @Nullable
    public Task findOne(String taskId, String userId);

    @NotNull
    public List<Task> findByName(String taskName, String userId);

    @NotNull
    public List<Task> findByNameOrDesc(String searchPhrase, String userId);

    @NotNull
    public List<Task> findByDescription(String description, String userId);

    public boolean contains(String taskId, String userId);

}
