package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    public User findUser(String login);

    public void removeAll();

    public boolean contains(String login);

}
