package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.util.Collection;

public interface IStateService {

    @NotNull
    public Collection<AbstractCommand> getCommands();

    @NotNull
    public User getCurrentUser() throws InterruptOperationException;

    public void setCurrentUser(User user);

    public boolean isAllowedCommand(Role ... roles);

    @Nullable
    public AbstractCommand getCommand(String operation);

}
