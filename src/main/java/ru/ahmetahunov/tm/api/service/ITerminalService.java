package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface ITerminalService {

    @NotNull
    public String readMessage() throws IOException;

    public void writeMessage(String message);

    @NotNull
    public String getAnswer(String question) throws IOException;

    public void close() throws IOException;

}
