package ru.ahmetahunov.tm.command.data.load;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.dto.Domain;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public class DataLoadJacksonXmlCommand extends AbstractCommand {

	@Override
	public boolean isSecure() {
		return false;
	}

	@NotNull
	@Override
	public String getName() {
		return "data-load-jackson-xml";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Load data from xml file using Jackson.";
	}

	@Override
	public void execute() throws Exception {
		@NotNull final Path dir = Paths.get("../data");
		@NotNull final Path file = Paths.get("../data/repo_jackson.xml");
		if (!Files.exists(dir) || !Files.exists(file)) throw new InterruptOperationException("File does not exist.");
		@NotNull final XmlMapper mapper = new XmlMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		@NotNull final Domain data = mapper.readValue(file.toFile(), Domain.class);
		data.upload(serviceLocator);
		serviceLocator.getTerminalService().writeMessage("[LOAD COMPLETE]");
	}

	@Nullable
	@Override
	public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}
