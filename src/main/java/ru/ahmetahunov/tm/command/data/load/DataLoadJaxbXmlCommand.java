package ru.ahmetahunov.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBUnmarshaller;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.eclipse.persistence.oxm.MediaType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.dto.Domain;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public class DataLoadJaxbXmlCommand extends AbstractCommand {

	@Override
	public boolean isSecure() {
		return false;
	}

	@NotNull
	@Override
	public String getName() {
		return "data-load-jaxb-xml";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Load data from xml file (JAXB).";
	}

	@Override
	public void execute() throws Exception {
		@NotNull final Path dir = Paths.get("../data");
		@NotNull final Path file = Paths.get("../data/repo_jaxb.xml");
		if (!Files.exists(dir) || !Files.exists(file)) throw new InterruptOperationException("File does not exist.");
		@NotNull final FileInputStream fis = new FileInputStream(file.toFile());
		@NotNull final JAXBContext context =
				(JAXBContext) JAXBContextFactory.createContext(new Class[]{Domain.class}, null);
		@NotNull final JAXBUnmarshaller unmarshaller = context.createUnmarshaller();
		unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_XML);
		@NotNull final Domain data = (Domain) unmarshaller.unmarshal(fis);
		data.upload(serviceLocator);
		fis.close();
		serviceLocator.getTerminalService().writeMessage("[LOAD COMPLETE]");
	}

	@Nullable
	@Override
	public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}
