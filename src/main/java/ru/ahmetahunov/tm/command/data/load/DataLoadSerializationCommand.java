package ru.ahmetahunov.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.dto.Domain;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public class DataLoadSerializationCommand extends AbstractCommand {

	@Override
	public boolean isSecure() {
		return false;
	}

	@NotNull
	@Override
	public String getName() {
		return "data-load";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Load data from binary file.";
	}

	@Override
	public void execute() throws Exception {
		@NotNull final Path dir = Paths.get("../data");
		@NotNull final Path file = Paths.get("../data/repositories.bin");
		if (!Files.exists(dir) || !Files.exists(file)) throw new InterruptOperationException("File does not exist.");
		@NotNull final FileInputStream fis = new FileInputStream(file.toFile());
		@NotNull final ObjectInputStream reader = new ObjectInputStream(fis);
		@NotNull final Domain data = (Domain) reader.readObject();
		data.upload(serviceLocator);
		reader.close();
		fis.close();
		serviceLocator.getTerminalService().writeMessage("[LOAD COMPLETE]");
	}

	@Nullable
	@Override
	public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}
