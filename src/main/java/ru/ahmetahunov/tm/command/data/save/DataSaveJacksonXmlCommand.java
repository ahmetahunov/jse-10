package ru.ahmetahunov.tm.command.data.save;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.dto.Domain;
import ru.ahmetahunov.tm.enumerated.Role;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

@NoArgsConstructor
public class DataSaveJacksonXmlCommand extends AbstractCommand {

	@Override
	public boolean isSecure() {
		return false;
	}

	@NotNull
	@Override
	public String getName() {
		return"data-save-jackson-xml";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Save all repositories like xml file (Jackson).";
	}

	@Override
	public void execute() throws Exception {
		@NotNull final Path dir = Paths.get("../data");
		if (!Files.exists(dir)) Files.createDirectory(dir);
		@NotNull final Path file = Paths.get("../data/repo_jackson.xml");
		if (!Files.exists(file)) Files.createFile(file);
		@NotNull final XmlMapper mapper = new XmlMapper();
		@NotNull final Domain data = new Domain();
		data.load(serviceLocator);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
		mapper.writerWithDefaultPrettyPrinter().writeValue(file.toFile(), data);
		serviceLocator.getTerminalService().writeMessage("[SAVED]");
	}

	@Nullable
	@Override
	public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}
