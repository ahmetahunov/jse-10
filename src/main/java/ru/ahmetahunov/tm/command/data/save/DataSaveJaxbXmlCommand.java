package ru.ahmetahunov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBMarshaller;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.oxm.MediaType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.dto.Domain;
import ru.ahmetahunov.tm.enumerated.Role;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public class DataSaveJaxbXmlCommand extends AbstractCommand {

	@Override
	public boolean isSecure() {
		return false;
	}

	@NotNull
	@Override
	public String getName() {
		return"data-save-jaxb-xml";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Save all repositories like xml file (JAXB).";
	}

	@Override
	public void execute() throws Exception {
		@NotNull final Path dir = Paths.get("../data");
		if (!Files.exists(dir)) Files.createDirectory(dir);
		@NotNull final Path file = Paths.get("../data/repo_jaxb.xml");
		if (!Files.exists(file)) Files.createFile(file);
		@NotNull final FileOutputStream fos = new FileOutputStream(file.toFile());
		@NotNull final JAXBContext context =
				(JAXBContext) JAXBContextFactory.createContext(new Class[]{Domain.class}, null);
		@NotNull final JAXBMarshaller marshaller = context.createMarshaller();
		@NotNull final Domain data = new Domain();
		data.load(serviceLocator);
		marshaller.setProperty(JAXBMarshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_XML);
		marshaller.marshal(data, fos);
		fos.close();
		serviceLocator.getTerminalService().writeMessage("[SAVED]");
	}

	@Nullable
	@Override
	public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}
