package ru.ahmetahunov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.dto.Domain;
import ru.ahmetahunov.tm.enumerated.Role;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@NoArgsConstructor
public class DataSaveSerializationCommand extends AbstractCommand {

	@Override
	public boolean isSecure() {
		return false;
	}

	@NotNull
	@Override
	public String getName() {
		return "data-save";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Save all repositories like binary file.";
	}

	@Override
	public void execute() throws Exception {
		@NotNull final Path dir = Paths.get("../data");
		if (!Files.exists(dir)) Files.createDirectory(dir);
		@NotNull final Path file = Paths.get("../data/repositories.bin");
		if (!Files.exists(file)) Files.createFile(file);
		@NotNull final FileOutputStream fos = new FileOutputStream(file.toFile());
		@NotNull final ObjectOutputStream stream = new ObjectOutputStream(fos);
		@NotNull final Domain data = new Domain();
		data.load(serviceLocator);
		stream.writeObject(data);
		stream.flush();
		stream.close();
		fos.close();
		serviceLocator.getTerminalService().writeMessage("[SAVED]");
	}

	@Nullable
	@Override
	public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}
