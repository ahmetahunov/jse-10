package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectByNameCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-by-name";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects with entered part of name.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[PROJECTS BY NAME]");
        @NotNull final String projectName = terminalService.getAnswer("Please enter project name: ");
        @NotNull final List<Project> projects = projectService.findByName(projectName, user.getId());
        int i = 1;
        for (@NotNull Project project : projects) {
            terminalService.writeMessage(String.format("%d.%s ID:%s", i++, project.getName(), project.getId()));
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
