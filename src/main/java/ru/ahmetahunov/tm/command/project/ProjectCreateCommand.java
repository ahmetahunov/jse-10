package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.DateUtil;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[PROJECT CREATE]");
        @Nullable final Project project = createNewProject();
        if (project == null) throw new InterruptOperationException("[FAILED]");
        terminalService.writeMessage("[OK]");
        terminalService.writeMessage(InfoUtil.getItemInfo(project));
    }

    @Nullable
    private Project createNewProject() throws IOException, InterruptOperationException {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String name = terminalService.getAnswer("Please enter project name: ");
        if (name.isEmpty()) throw new InterruptOperationException("Name cannot be empty.");
        @NotNull final String description = terminalService.getAnswer("Please enter description:");
        @NotNull final String startDate =
                terminalService.getAnswer("Please insert start date(example: 01.01.2020): ");
        @NotNull final String finishDate =
                terminalService.getAnswer("Please enter finish date(example: 01.01.2020): ");
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setStartDate(DateUtil.parseDate(startDate));
        project.setFinishDate(DateUtil.parseDate(finishDate));
        project.setUserId(user.getId());
        return serviceLocator.getProjectService().persist(project);
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}