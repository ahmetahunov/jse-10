package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectEditDescriptionCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-edit-desc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit description of project";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT DESCRIPTION]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final Project project = serviceLocator.getProjectService().findOne(projectId, user.getId());
        if (project == null) throw new InterruptOperationException("Selected project does not exist");
        @NotNull final String description = terminalService.getAnswer("Please enter new description: ");
        project.setDescription(description);
        serviceLocator.getProjectService().merge(project);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}
