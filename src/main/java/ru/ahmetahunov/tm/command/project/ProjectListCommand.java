package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.ComparatorUtil;
import java.io.IOException;
import java.util.Comparator;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all available projects";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        int i = 1;
        terminalService.writeMessage("[PROJECT LIST]");
        @NotNull final String comparatorName = terminalService.getAnswer(
                "Enter sort type<creationDate|startDate|finishDate|status>: "
        );
        @NotNull final Comparator<Project> comparator = ComparatorUtil.getComparator(comparatorName);
        for (@NotNull Project project : projectService.findAll(user.getId(), comparator)) {
            terminalService.writeMessage(String.format("%d. %s ID:%s", i++, project.getName(), project.getId()));
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}