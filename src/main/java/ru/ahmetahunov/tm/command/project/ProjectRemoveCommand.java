package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        terminalService.writeMessage("[PROJECT REMOVE]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final Project removedProject = serviceLocator.getProjectService().remove(projectId, user.getId());
        if (removedProject == null) throw new InterruptOperationException("Selected project does not exist.");
        serviceLocator.getTaskService().removeAllProjectTasks(projectId, user.getId());
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}