package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectSelectCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "project-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show selected project with tasks.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        terminalService.writeMessage("[PROJECT SELECT]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final Project project = serviceLocator.getProjectService().findOne(projectId, user.getId());
        if (project == null) throw new InterruptOperationException("Selected project does not exist.");
        terminalService.writeMessage(project.getName() + ":");
        int i = 1;
        for (@NotNull Task task : taskService.findAll(project.getId(), user.getId())) {
            terminalService.writeMessage(String.format("  %d. %s", i++, task.getName()));
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}
