package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.DateUtil;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[TASK CREATE]");
        @Nullable final Task task = createNewTask();
        if (task == null) throw new InterruptOperationException("[FAILED]");
        terminalService.writeMessage("[OK]");
        terminalService.writeMessage(InfoUtil.getItemInfo(task));
    }

    @Nullable
    private Task createNewTask() throws IOException, InterruptOperationException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String projectId = terminalService.getAnswer("Enter project id: ");
        @NotNull final String name = terminalService.getAnswer("Please enter task name: ");
        if (name.isEmpty()) throw new InterruptOperationException("Name cannot be empty.");
        @NotNull final String description = terminalService.getAnswer("Please enter description: ");
        @NotNull final String startDate =
                terminalService.getAnswer("Please enter start date(example: 01.01.2020): ");
        @NotNull final String finishDate =
                terminalService.getAnswer("Please enter finish date(example: 01.01.2020): ");
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(DateUtil.parseDate(startDate));
        task.setFinishDate(DateUtil.parseDate(finishDate));
        task.setProjectId(projectId);
        task.setUserId(serviceLocator.getStateService().getCurrentUser().getId());
        return serviceLocator.getTaskService().persist(task);
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}