package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.IOException;

@NoArgsConstructor
public final class TaskEditDescriptionCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-edit-descr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit description of selected task.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT DESCRIPTION]");
        @NotNull final String taskId = terminalService.getAnswer("Please enter task id: ");
        @Nullable final Task task = serviceLocator.getTaskService().findOne(taskId, user.getId());
        if (task == null) throw new InterruptOperationException("Selected task does not exist.");
        @NotNull final String description = terminalService.getAnswer("Please enter new description: ");
        task.setDescription(description);
        serviceLocator.getTaskService().merge(task);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}
