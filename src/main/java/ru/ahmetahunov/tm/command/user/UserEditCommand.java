package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.PassUtil;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class UserEditCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user's information.";
    }

    @Override
    public void execute() throws IOException, NoSuchAlgorithmException, InterruptOperationException {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[USER EDIT]");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        password = PassUtil.getHash(password);
        if (!user.getPassword().equals(password)) throw new InterruptOperationException("Wrong password!");
        @NotNull final String login = terminalService.getAnswer("Please enter new login: ");
        if (userService.contains(login)) throw new InterruptOperationException("User with this login already exists!");
        user.setLogin(login);
        serviceLocator.getUserService().merge(user);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}
