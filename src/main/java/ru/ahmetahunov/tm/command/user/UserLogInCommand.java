package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.PassUtil;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "log-in";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException, NoSuchAlgorithmException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[LOG IN]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @Nullable final User user = serviceLocator.getUserService().findUser(login);
        if (user == null) throw new InterruptOperationException("Selected user does not exist.");
        @NotNull final String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String passHash = PassUtil.getHash(password);
        if (!user.getPassword().equals(passHash)) throw new InterruptOperationException("Wrong password.");
        serviceLocator.getStateService().setCurrentUser(user);
        terminalService.writeMessage("Welcome, " + user.getLogin());
    }

}
