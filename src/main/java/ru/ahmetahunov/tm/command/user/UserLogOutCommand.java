package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.enumerated.Role;

@NoArgsConstructor
public final class UserLogOutCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "log-out";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Log out.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[LOG OUT]");
        serviceLocator.getStateService().setCurrentUser(null);
        terminalService.writeMessage("Have a nice day!");
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
