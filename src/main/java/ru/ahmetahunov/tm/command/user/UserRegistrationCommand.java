package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.PassUtil;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class UserRegistrationCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "user-register";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "New user registration.";
    }

    @Override
    public void execute() throws IOException, InterruptOperationException, NoSuchAlgorithmException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminalService.writeMessage("[REGISTRATION]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        if (userService.contains(login))
            throw new InterruptOperationException("This login already exists. Try one more time!");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new InterruptOperationException("Passwords do not match!");
        password = PassUtil.getHash(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        userService.persist(user);
        terminalService.writeMessage("[OK]");
    }

}
