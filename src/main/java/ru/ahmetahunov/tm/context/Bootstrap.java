package ru.ahmetahunov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.*;
import ru.ahmetahunov.tm.command.*;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.repository.UserRepository;
import ru.ahmetahunov.tm.service.*;
import ru.ahmetahunov.tm.util.PassUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IStateService stateService = new StateService(commands);

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService(reader);

    public void init() throws Exception {
        initCommands();
        createDefaultUsers();
        startCycle();
    }

    private void initCommands() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.ahmetahunov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> commandClasses =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull Class<? extends AbstractCommand> commandClass : commandClasses) {
            @NotNull final AbstractCommand command = commandClass.getDeclaredConstructor().newInstance();
            command.setServiceLocator(this);
            commands.put(command.getName(), command);
        }
    }

    private void startCycle() throws IOException {
        terminalService.writeMessage( "*** WELCOME TO TASK MANAGER ***" );
        @NotNull String operation = "";
        while (!"exit".equals(operation)) {
            try {
                operation = terminalService.getAnswer("Please enter command: ").toLowerCase();
                @Nullable final AbstractCommand command = stateService.getCommand(operation);
                if (command == null) continue;
                command.execute();
            }
            catch (InterruptOperationException e) { terminalService.writeMessage(e.getMessage()); }
            catch (Exception e) { e.printStackTrace(); }
            terminalService.writeMessage("");
        }
        terminalService.close();
    }

    private void createDefaultUsers() throws NoSuchAlgorithmException {
        @NotNull final User user = new User();
        user.setId("64a8bae7-e916-4d1d-9b1e-04bc19d0bd91");
        user.setLogin("user");
        user.setPassword(PassUtil.getHash("0000"));
        userService.persist(user);
        @NotNull final User admin = new User();
        admin.setId("3d8043c1-6aeb-4df9-af8d-7a8de0f99a09");
        admin.setLogin("admin");
        admin.setPassword(PassUtil.getHash("admin"));
        admin.setRole(Role.ADMINISTRATOR);
        userService.persist(admin);
    }

}
