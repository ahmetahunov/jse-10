package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.enumerated.Role;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity implements Serializable {

    @NotNull
    private String login = "";

    @NotNull
    private String password = "";

    @NotNull
    private Role role = Role.USER;

}
