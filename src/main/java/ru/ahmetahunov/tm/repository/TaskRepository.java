package ru.ahmetahunov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.entity.Task;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeAll(@NotNull final String userId) {
        for (@NotNull Task task : findAll(userId))
            remove(task.getId());
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> tasks = new LinkedList<>();
        for (@NotNull Task task : findAll()) {
            if (task.getUserId().equals(userId)) tasks.add(task);
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator) {
        @NotNull final List<Task> tasks = new LinkedList<>();
        for (@NotNull Task task : findAll()) {
            if (task.getUserId().equals(userId)) tasks.add(task);
        }
        tasks.sort(comparator);
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String projectId, @NotNull final String userId) {
        @NotNull final List<Task> tasks = new LinkedList<>();
        for (@NotNull Task task : findAll()) {
            if (task.getUserId().equals(userId) && task.getProjectId().equals(projectId))
                tasks.add(task);
        }
        return tasks;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String taskId, @NotNull final String userId) {
        @Nullable Task task = collection.get(taskId);
        if (task == null || !task.getUserId().equals(userId)) return null;
        return task;
    }

    @NotNull
    @Override
    public List<Task> findByName(@NotNull final String taskName, @NotNull final String userId) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull Task task : findAll()) {
            if (task.getUserId().equals(userId) && task.getName().contains(taskName))
                tasks.add(task);
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByDescription(@NotNull final String description, @NotNull final String userId) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull Task task : findAll()) {
            if (task.getUserId().equals(userId) && task.getDescription().contains(description))
                tasks.add(task);
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDesc(@NotNull final String searchPhrase, @NotNull final String userId) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull Task task : findAll()) {
            if (!task.getUserId().equals(userId)) continue;
            if (task.getName().contains(searchPhrase) || task.getDescription().contains(searchPhrase))
                tasks.add(task);
        }
        return tasks;
    }

    @Override
    public boolean contains(@NotNull final String taskId, @NotNull final String userId) {
        @Nullable final Task task = collection.get(taskId);
        if (task == null) return false;
        return task.getUserId().equals(userId);
    }

}
