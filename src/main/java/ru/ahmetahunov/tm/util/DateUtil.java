package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

    @NotNull
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static Date parseDate(@NotNull final String date) {
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    @NotNull
    public static String formatDate(@Nullable final Date date) {
        if (date == null || date.equals(new Date(0))) return "not set";
        return dateFormatter.format(date);
    }

}
