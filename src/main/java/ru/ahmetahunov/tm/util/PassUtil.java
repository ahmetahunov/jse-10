package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class PassUtil {

    @NotNull
    public static String getHash(@NotNull final String password) throws NoSuchAlgorithmException {
        @NotNull final MessageDigest encoder = MessageDigest.getInstance("MD5");
        @NotNull final String salt = "$ert4!";
        @NotNull String result = password;
        for (int i = 0; i < 10; i++) {
            encoder.update((salt + result).getBytes());
            @NotNull byte[] buff = encoder.digest();
            result = new BigInteger(1, buff).toString(16);
            encoder.update((result + salt).getBytes());
            buff = encoder.digest();
            result = new BigInteger(1, buff).toString(16);
        }
        return result;
    }

}
