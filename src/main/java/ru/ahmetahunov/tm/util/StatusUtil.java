package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.enumerated.Status;

public final class StatusUtil {

    @Nullable
    public static Status getStatus(@NotNull final String status) {
        if ("planned".equals(status.toLowerCase())) return Status.PLANNED;
        if ("in-progress".equals(status.toLowerCase())) return Status.IN_PROGRESS;
        if ("done".equals(status.toLowerCase())) return Status.DONE;
        return null;
    }

}
